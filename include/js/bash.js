

/**
 *  add commands and examples to the array here below
 */
const bashCommands = [
    { //bash command
        command: "ls",
        desc: "list<br>Birtir lista af skrám sem eru í núverandi möppu",
        examples: []
    },
    { //bash command
        command: "mv",
        desc: "move file<br>Færir skrár, en getur líka verið notað til að endurskýra skrár",
        examples: [
            {
                code: "mv skra1 mappa/skra1",
                desc: "færir 'skra1' í undirmöppuna 'mappa'"
            },
            {
                code: "mv skra1 skra2",
                desc: "endurskýrir 'skra1' sem 'skra2'"
            }
        ]
    },
    { //bash command
        command: "cat",
        desc: "concatenate and print files<br>'Prentar' innhald skjala beint í terminal. Það getur einnig afritað skjöl, en heldur ekki séríslenskum stöfum í afritum.",
        examples: [
            {
                code: "cat skjal",
                desc: "prentar innihald 'skjal' í terminalið",
            },
            {
                code: "cat skjal1 > skjal2",
                desc: "afritar 'skjal1' sem 'skjal2'",
            },
            {
                code: "cat skjal1 file2 > file3",
                desc: "sameinar innihald 'skjal1' og 'skjal2' sem 'skjal3'",
            }
        ]
    },
    { //bash command
        command: "cd",
        desc: "concatenate and print files<br>'Prentar' innhald skjala beint í terminal. Það getur einnig afritað skjöl, en heldur ekki séríslenskum stöfum í afritum.",
        examples: [
            {
                code: "cd ..",
                desc: "fara upp úr núverandi möppu (eitt skref til baka)",
            },
            {
                code: "cd /dirname",
                desc: "fara í möppu sem heitir dirname sem er í núverandi möppu",
            },
            {
                code: "cd /",
                desc: "fara alla leið til baka í grunn möppuna 'C:\'",
            }
        ]
    },
    { //bash command
        command: "mkdir",
        desc: "mkdir<br>Býr til nýjan directory í tölvunni þinni",
        examples: [
            {
                code: "mkdir -p",
                desc: "-p er parents eða path, og býr til directory sem leiðir að directoryinum sem þú ert að búa til",
            },
            {
                code: "mkdir -m mode",
                desc: "setur nýjan directory sem þú bjóst til í ákveðið mode",
            }
        ]
    }
    
//Add new commands here above to the array list, remember to add "," between objects
]

if(this !== window){
    module.exports = {
        bashCommands
    }
}