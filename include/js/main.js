//function to generate a list of bash commands and examples
//need to redo the <ul>, because there appears a , between list items

const listBashCommands = ()=>{
    const bashSection = document.getElementById("list-bash");
    bashSection.innerHTML = "<h2>Bash skipanir</h2>"
    bashCommands.map((item)=>{ //hvaða command?
        let content = `
            <p><span class="code">${item.command}</span> //${item.desc}</p>
            <ul>
        `;
        item.examples.map((example)=>{ //birta examples
            content += `<li><span class="code">${example.code}</span> //${example.desc}</li>`;
        }) 
        //loka examples listanum
        content += "</ul>"
        bashSection.innerHTML += content
    })
}


//render the list with the bash commands listed in the array above
listBashCommands()