document.body.innerHTML = '<section id="list-bash"></section>'; //býr til innerHTML fyrir testið
const { bashCommands } = require("../include/js/bash.js");
test('Each bash object has to have these properties: "command", "desc" and "examples"', ()=>{
    bashCommands.forEach((command)=>{
        expect(command).toHaveProperty('command')
        expect(command).toHaveProperty('desc')
        expect(command).toHaveProperty('examples')
    })
})

test('The "command" in the bash object needs to be a STRING', ()=>{
    bashCommands.forEach((command)=>{
        expect(typeof command.command).toBe('string')
    })
})

test('The "desc" in the bash object needs to be a STRING', ()=>{
    bashCommands.forEach((command)=>{
        expect(typeof command.desc).toBe('string')
    })
})

test('The "examples" in the bash object needs to be an ARRAY', ()=>{
    bashCommands.forEach((command)=>{
        expect(Array.isArray([command.examples])).toBe(true)
    })
})

test('The command "desc" needs to contain a <br>', ()=>{
    bashCommands.forEach((command)=>{
        expect(command.desc).toMatch(/<br>/)
    })
})